//
//  Track.swift
//  itunesSearch
//
//  Created by Risalba on 23/11/2016.
//  Copyright © 2016 Risalba. All rights reserved.
//

import Foundation

struct Track {
    
    let artistName:String
    let album:String
    let trackName:String
    let releaseDate:Date
    let price:Double
    let currency:String
    let artworkURL:URL
    static let dateFormatter:DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSS'Z'"
        return dateFormatter
    }()
    
    init(withDictionary dictionary:[String:Any]) {
        artistName = dictionary["artistName"] as! String? ?? ""
        album = dictionary["collectionName"] as! String? ?? ""
        trackName = dictionary["trackName"] as! String?  ?? ""
        let releaseDate = dictionary["releaseDate"] as! String? ?? ""
        self.releaseDate = Track.dateFormatter.date(from: releaseDate) as Date? ?? Date()
        price = dictionary["trackPrice"] as! Double? ?? 0
        currency = dictionary["currency"] as! String? ?? ""
        let artworkUrl100 = dictionary["artworkUrl100"] as! String? ?? ""
        artworkURL = URL(string: artworkUrl100)!
    }
}
