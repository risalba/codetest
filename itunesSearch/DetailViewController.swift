//
//  DetailViewController.swift
//  itunesSearch
//
//  Created by Risalba on 22/11/2016.
//  Copyright © 2016 Risalba. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    
    @IBOutlet weak var imageView: RemoteImageView!
    @IBOutlet weak var labelArtist: UILabel!
    @IBOutlet weak var labelAlbum: UILabel!
    @IBOutlet weak var labelTrackName: UILabel!
    @IBOutlet weak var labelReleaseDate: UILabel!
    @IBOutlet weak var labelPrice: UILabel!

    static let dateFormatter:DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        return dateFormatter
    }()
    
    var detailItem: Track? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }
    
    func configureView() {
        // Update the user interface for the detail item.
        if let detail = self.detailItem,
            let _ = self.labelArtist
        {
            self.labelArtist.text = detail.artistName
            self.labelAlbum.text = detail.album
            self.labelTrackName.text = detail.trackName
            self.labelReleaseDate.text = DetailViewController.dateFormatter.string(from: detail.releaseDate)
            self.labelPrice.text = "\(detail.price) \(detail.currency)"
            self.imageView.loadImage(fromURL: detail.artworkURL)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        imageView.clipsToBounds = true
        self.configureView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        imageView.layer.cornerRadius = imageView.frame.size.width / 2
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

