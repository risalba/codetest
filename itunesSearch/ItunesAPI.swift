//
//  ItunesAPI.swift
//  itunesSearch
//
//  Created by Risalba on 24/11/2016.
//  Copyright © 2016 Risalba. All rights reserved.
//

import Foundation
import UIKit

class ItunesAPI {
    static let sharedInstance = ItunesAPI()
    let session = URLSession(configuration: URLSessionConfiguration.default)
    let opetationQueue = OperationQueue()
    var latestSearchOperation:Operation?
    
    func search(withString string:String, completionBlock: @escaping ([Track]) -> Void) {
        
        latestSearchOperation?.cancel()
        
        let searchOperation = BlockOperation()
        searchOperation.addExecutionBlock {
            [weak self] in
            let searchText = string.replacingOccurrences(of: " ", with: "+")
            let url = URL(string: Config.urlRoot + searchText)
            self?.session.dataTask(with: url!) { (data, response, error) -> Void in
                
                if !searchOperation.isCancelled, let response = response as? HTTPURLResponse, 200...299 ~= response.statusCode, let data = data {
                    
                    let json = try? JSONSerialization.jsonObject(with: data)
                    if let json = json as? [String:Any] {
                        
                        if !searchOperation.isCancelled {
                            OperationQueue.main.addOperation {
                                let objects = json["results"] as! [[String:Any]]
                                let tracks = objects.map{
                                    Track(withDictionary: $0)
                                }
                                completionBlock(tracks)
                            }
                        }
                    } else {
                        
                    }
                }
            }.resume()
        }
        latestSearchOperation = searchOperation
        opetationQueue.addOperation(searchOperation)
    }
}
