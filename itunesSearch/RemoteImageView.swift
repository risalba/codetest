//
//  RemoteImageView.swift
//  itunesSearch
//
//  Created by Risalba on 24/11/2016.
//  Copyright © 2016 Risalba. All rights reserved.
//

import UIKit

class RemoteImageView: UIImageView {

    static let opetationQueue = OperationQueue()
    static let session = URLSession(configuration: URLSessionConfiguration.default)
    var latestDownloadOperation:Operation?
    
    func loadImage(fromURL URL:URL) {

        latestDownloadOperation?.cancel()
        
        let downloadOperation = BlockOperation()
        downloadOperation.addExecutionBlock {
            [weak self] in
            RemoteImageView.session.dataTask(with: URL) { (data, response, error) -> Void in
                if let data = data, let image = UIImage(data: data), !downloadOperation.isCancelled {
                    OperationQueue.main.addOperation {
                        self?.image = image
                    }
                }
            }.resume()
        }
        RemoteImageView.opetationQueue.addOperation(downloadOperation)
        latestDownloadOperation = downloadOperation
    }
}
