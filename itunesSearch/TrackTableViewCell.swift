//
//  TrackTableViewCell.swift
//  itunesSearch
//
//  Created by Risalba on 24/11/2016.
//  Copyright © 2016 Risalba. All rights reserved.
//

import UIKit

class TrackTableViewCell: UITableViewCell {

    @IBOutlet var coverImageView: RemoteImageView!
    @IBOutlet var artistLabel: UILabel!
    @IBOutlet var trackNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
