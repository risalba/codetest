# README #

### CODETEST ###

This is a simple Master-Detail view application in Swift. I used the default template for Master-Detail applications from Xcode,so it supports iPhone and iPad and even gives some extra customisation for iPhone 6/7 Plus.

Since the aim of this project was to prove that I can make the app, and there was left  space to write about how would the ideal implementation be I opted for developing a rapid functional prototype and iterate over it. I think in cases when the time is limited is a good approach to show a functional and almost finished (looking) product.


### UPDATE ###

The following unreleased tasks have been finished:

1. I would had wrapped the API calls in a class or struct that would have received the queries and return the result of the query in an execution blocked.
2. I would have made in the same wrapper a function receiving an URL and returning a UIImage in an execution block
3. I would have made an special version of that call that allows me to control if the image downloaded should be displayed on the cell based on the indexPath of the cell that may have changed because of scrolling.
4. I would create a struct to model the contents of a track, the struct could have an initialiser that receives a node of the JSON response and creates an instance unwrapping the values or setting a default value if by any chance those were not available
5. I would have made the interface more responsive to size classes, currently is slightly  optimised for iPhone 7 portrait and landscape modes, but it could also adapt better for iPad and I should test how's displayed in smaller phones or when the track title is too long.

NOTES:

* The approach to points 2 and 3 is not in the API wrapper, and has been instead implemented using an UIImageView subclass.
* GCD calls have been refactored using NSOperationQueue and NSOperation (NSBlockOperation in this case) to allow cancelling of the tasks.
* made results be updated as the user types the search